// BASE SETUP
// =============================================================================

// call the packages we need
const express  = require('express');        // call express
let app        = express();                 // define our app using express
let router     = express.Router();
const bodyParser = require('body-parser');
const cors     = require('cors');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const port = process.env.PORT || 8081;        // set our port

// more routes for our API will happen here
const routerApi = require('./routes/index');
const routerSimpleForm = require('./routes/sample-form-response'); 

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api/v1
app.use('/api/v1', routerApi);
app.use('/api/v1/sample', routerSimpleForm);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);